;    Vezza: Z-Code interpreter for the Z80 processor
;    Z1/2/6/7 Extensions, bugfixes, rewrites and optimisations
;    Copyright (c) 2021-2024 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on original ZXZVM:
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;    Portions copyright 2019 Garry Lancaster
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;

;	include	in_zxzvm.inc	;included in the main build file -has #DEFINE statements for platform config

;peek64	equ	ZXPK64


	org	VMORG	;Program Segment Prefix (aka Zero Page).

;page-aligned tables to save cycles on checking for 8 bit overflow. Requires VMorg is xx00H
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;0OPs
;

zero_ops:	;0OP: 0   1     2      3      4       5       6        7  
	defw	rtrue,  rfalse, print, prret, no_op,  d_save0, d_restr0, restart
    defw    rpop,   catch,  zquit, newln, sstatus,vrfy,   ext_inst,    piracy
; catch is replaced in the table by vpop during load if zver<5
ext_inst_fix:	EQU	$-4		;Aren't allowed extended opcodes in v1-v4, replaces with fail in loader.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
one_ops:	;1OP: 0     1      2      3     4         5     6       7
	defw	 z_jz,   z_os,  z_oc,  z_op, prplen, incvar, decvar, praddr
    defw     call1s, rmobj, probj, z_rv, z_jmp,  printp, loadv,  call1n
; call 1n for zver <5 replced with znot during load
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
two_ops:	;2OP: 0      1       2       3       4      5      6      7
	defw	zbreak,   z_je,   z_jl,   z_jg, z_dck, z_ick,  z_jin, testv
        defw      z_or,  z_and,   z_ta,   z_sa,  z_ca, store, insobj, z_ldw
        defw     z_ldb,   z_gp,  z_gpa,  z_gnp, z_add, z_sub,  z_mul, z_div
;        defw     z_mod, call2s, call2n, z_scol, throw,  no_op,   no_op,  no_op
        defw     z_mod, call2s, call2n, z_scol, throw,  fail,   fail,  fail
; z_jin gets replaced with v3jin during setup stage if zver <=3
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
ext_ops:	;0       1        2       3       4      5        6        7
IFDEF V6routines
	defw	d_save,  d_restr, z_srl,  z_sra,  sfont, drawpic, picdata, erapic
	defw	smargin, u_save,  u_restr,pruni,  ckuni, fail,    fail,    fail
	defw	movewin, winsize, winstyl,getwinp,scrolw,popstack,readm,   mousewin
	defw	pushstak,putwinp, printfrm,makemenu,pictbl,buffscr,fail,   fail
; The bottom 2 rows are V6 only opcodes
;
MAXEXT	EQU	1Fh	;Maximum extended opcode.
ELSE
	defw	d_save,  d_restr, z_srl,  z_sra,  sfont, fail,    fail,    fail
	defw	fail  , u_save,  u_restr,pruni,  ckuni, fail,    fail,    fail

;
MAXEXT	EQU	0Fh	;Maximum extended opcode.
ENDIF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
var_ops:	;VAR:0         1         2      3        4       5      6    7
	defw	call_vs, zstorew, zstoreb,zpprop,  zread0, print_char, prnum,z_rand
	defw	zput,    zpull,   split_w, sel_w,call_vs2, erase_w,erase_l,setcur
	defw	getcpos, style,   sbfmde,  ostrm,  istrm, sfx, read_char, scantbl
	defw	znot0,    call_vn, call_vn2, ztok, zencode, cpytab, prtbl,ck_arg
; zpull is replaced by zpull_v6 at load if required.
; setcur is replaces with setcur_v6 at load if required.
;

;zinum:	defb	7	;<< v0.02 >> Pretend to be a C128
;zinum:	defb	1	; Model 4 - pretend to be a VT-220 to allow font choice in BZ
zinum:	equ	1	; Model 4 - pretend to be a VT-220 to allow font choice in BZ

;param	equ	VMORG+5Ch	;Where the FCB parameter would be.

;	org	VMORG+100h
main:
;;TESTING	ld	(isp),sp	;Save BASIC's SP

;;	ld	hl,0
;;	ld	(cycles),hl
;;	ld	de,param
	call	ZXINIT	;Initialise the I/O code
	jp	nc,syserr_msg
IFDEF	UnseenError	
	call	ZXVER
	ld	hl,vererr
	cp	VMVER
	jp	nz,syserr_msg
ENDIF
	call	ZXIHDR
	jp	nc,syserr_msg
	call	init_hdr
;	jp	nc,syserr_msg	;nothing is tested.
	call	once_only	;configure self modifying code & test memory write
	jp	nc,syserr_msg	;memory write fails
IFDEF	DT80
;	jp	zmreset
;;        call    init_scr
;       jp      nc,syserr
;;        call    init_rnd
;;        call    init_stack
	call	init_all_env
        call    ZXRST		;Restart
        jp      nc,syserr
        call    ZXIHDR
        jp      nc,syserr
        call    init_hdr
ENDIF
zmreentry:
	call	init_all_env
;;	call	init_scr
;	jp	nc,syserr
;;	call	init_rnd
;	jp	nc,syserr	;no SCF test on random anymore.
;	call	test_mem
;	jp	nc,syserr_msg
;;	call	init_stack
;zi	jp	nc,syserr	;no SCF test on this
	ld	a,1
	ld	(running),a
IFDEF	DebugCode
	call	showpc		;might need to re-instate
ENDIF
;IFDEF	DT80
;	jp	zmreset
;ENDIF
zloop:	call	zinst
IFDEF ZX
	push	hl		;<< v0.02

	ld	hl,(cycles)
	inc	hl
	ld	(cycles),hl	;Call ZXRCPU once every 2048 z-cycles.

	bit	3,h
	jp	z,zlp0
	ld	hl,0
	ld	(cycles),hl
	call	ZXRCPU
zlp0:				; v0.02 >>
zlp1:	
	pop	hl
ENDIF
;	ld	a,(trace)
;	ld	a,0

;	or	a
;	call	nz,showpc
	ld	a,1
running:	EQU	$-1
trace:	equ	$-1	;; Set bit 7 to enable debug trace if DebugCode is compiled
;	ld	a,(running)	;Running = 1 to continue
	dec	a		;        = 0 to quit (or any other value - the previous stub code simply exited with error)
	jp	z,zloop		;        = 2 to restart - this line now a jump.
	inc	a
IFDEF	DebugCode
	call	m,showpc
ENDIF
	and	7fh
	dec	a
	jp	z,zloop
	dec	a
	jr	z,zmreset
	jr	zmexit
;	jp	stub
;
zmstop:
IFDEF	DebugCode
	call	showpc
ENDIF
	pop	de	;clean up stack from call zinst
	push	hl
	call	flush_buf
	call	showstk
	ld	de,anykey
	ld	c,9
	call	ZXFDOS
	ld	c,6
	ld	e,0FDh
	call	ZXFDOS
	pop	hl
	jp	syserr_msg
;
zmexit:	call	flush_buf
;	ld	de,anykey
;	ld	c,9
;	call	ZXFDOS
;	ld	c,6
;	ld	e,0FDh
;	call	ZXFDOS
	scf		;execute return at end which will take you initial VM main call
	jp	ZXEXIT	;graceful exit - de-init screen then return to mxzvm
;	call	ZXEXIT
;;TESTING       ld      sp,(isp)
;	ret			;<< v0.02 >> Spectrum-specific code removed
;				;from ZXZVM
zmreset:
        call    ZXRST		;Restart
        jp      nc,syserr
        call    ZXIHDR
        jp      nc,syserr
        call    init_hdr
        jp	zmreentry
;       jp      nc,syserr
;        call    init_scr
;        jp      nc,syserr
;        call    init_rnd
;;        jp      nc,syserr - no internal failure tests
;        call    init_stack
;;zi        jp      nc,syserr
;        ld      a,1
;        ld      (running),a
;	jp	zloop

;stub:	ld	a,8
;	ld	(5C3Ah),a
;	ld	hl,msg1
syserr:
	ld	hl,abandoned	;bugfix - message added
syserr_msg:
	xor	a
	call	ZXEXIT
;
;ZXEXIT should not return if called with Carry reset. But in case it does,
;      reboot!
;
	rst	0
;	di
;	halt
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Numerical data
;
;cycles:	defw	0	;No. of Z-machine cycles executed modulo 2048
;isp:	defw	0	;BASIC's SP
;zsp:	defw	0	;Z-machine stack pointer
zstop:	defw	0	;Top of Z-machine stack
rsp:	defw	0	;Routine stack pointer
;rstop:	defw	0	;Top of routine stack
;zpc:	defw	0,0	;Z-machine program counter
zipc:	defw	0	;Address of last opcode (as opposed to data)
	defb	0
inst:	defb	0,0	;Current instruction [2nd byte is EXT instruction]
zver:	defb	0	;Version of the Z-Machine
;running:
;	defb	0	;Set to 1 while the Z-machine is running
statusbar:
	defb	0	;Set to 1 on v3 when the status bar is enabled.
;
;Some strings
;
;msg1:	defb	'Stub encountere'
;	defb	0E4h		;'d'+80h
;
IFDEF	UnseenError
vererr:	defb	'Version mismatc'
	defb	0E8h		;'h'+80h
ENDIF

anykey:	defb	13,10,'Press SPACE to finish',13,10,'$'
;
IFDEF	DoubleCheck		;only makes sense to double-check if not compiling monolithic
zvbad:  defb    'Story type '
zvbuf:  defb    '000$'
ENDIF
;
;;;;;;;;;;;;;;;;;;; Set up the header ;;;;;;;;;;;;;;;;;;;;;
;
init_hdr:
	ld	e,0
	ld	h,e		;save a byte
	ld	l,e
;	ld	hl,0
	call	ZXPEEKi		;Get Z-file version
        ld      (zver),a
IFDEF	DoubleCheck
        ld      de,zvbuf
        ld      l,a
        ld      h,0             ;Create the "invalid version" error
        call    spdec3
        ex      de,hl
        dec     hl
        set     7,(hl)          ;Fatal error, so set bit 7 of last char
        ld      hl,zvbad
	ld	a,(zver)	;Get version
	or	a		;v1-5 and 7,8 are all supported
IFNDEF	V6routines
	jp	z,ihdr8	
	cp	6		;6 is not supported in this version
ENDIF
        jp      z,ihdr8
	cp	9
	jp	nc,ihdr8	;9 or higher is bad
ENDIF			;Double-check version number
verok:
	ld	hl,001Eh	;Interpreter number then release number
	ld	b,zinum		;The interpreter number we pretend to be
	ld	c,'I'		;Interpreter release number
	jp	zxpokw
;	call	zxpokw
;	scf
;	ret
;
IFDEF	DoubleCheck
ihdr8:  and     a
	ret
ENDIF
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; initialize screen, random numbers and stack - fall through each
init_all_env:
init_scr:
	ld	a,-1	;erase_window(-1)
	call	ZXERAW
	xor	a
	ld	(abbrev),a
	ld	(alpha),a
	ld	(dalpha),a
;	ld	(shift),a
	ld	(multi),a
	ld	(timer),a
	ld	(statusbar),a
;	jp	inibuf	;initialise I/O buffers.
	call	inibuf	;Initialise I/O buffering
;	scf
;	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Initialise the random number generator
;
init_rnd:
	xor	a
	ld	(rmode),a
;	ld	hl,0
	ld	h,a
	ld	l,a
	call	random	;Seed the generator
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Stack frame format (little-endian) 38 byte total:
;OLD:
;+0:  DD pc		;Previous ZPC
;+4:  DS 30		;Local variables
;+34: DB call type	;0=function, 1=procedure, 2=interrupt
;+35: DB pcount		;Count of parameters
;+36: DW rsp		;Routine stack pointer
;NEW:
;+0: DD pc		;Previous ZPC (EHL with a spare byte for historical use)
;+4: DB call type	;0=function, 1=procedure, 2=interrupt
;+5: DB pcount		;Count of parameters
;+6: DW rsp		;Routine stack pointer
;+8: DS 30		;Local variables
;
;NEWnew: - not worth the speed change. Also would need changed to save files. Left here for the record.
;+0: DD pc		;Previous ZPC - now only 3 bytes
;+3: DB call type	;0=function, 1=procedure, 2=interrupt
;+4: DB pcount		;Count of parameters
;+5: DW rsp		;Routine stack pointer
;+7: DS 30		;Local variables
;
init_stack:
	call	ZXTMEM
	inc	hl		;1st unusable byte
	ld	(zsp),hl
	ld	(zstop),hl
IFDEF	BigBuffers
	ld	de,-4096	;4k stack
ELSE
IFDEF	MedBuffers
	ld	de,-2560	;2.5k stack ;3072	;3k stack
ELSE
	ld	de,-2048	;2k call stack
ENDIF
ENDIF
	add	hl,de
	ld	(rsp),hl	;Routine stack
	ld	(rstop),hl
	call	mkframe		;Returns HL->frame. As this is the first we should consider
				;changing the starting location to be himem-37.

	ld	de,(rsp)
	xor	a
	ld	b,5	;+2 +2 +2 + 1 - 8 bytes - this save 1 byte
init_stack_lp:
	ld	(hl),a	;ZPC (3 bytes)
	inc	hl
	djnz	init_stack_lp
;	ld	(hl),a
;	inc	hl
;	ld	(hl),a
;	inc	hl
;	ld	(hl),a	;no longer used in 37 byte frame. Excess byte for DEHL.
;	inc	hl
;	ld	(hl),1	;procedure type
	inc	(hl)
	inc	hl
	ld	(hl),a	;number local variables
	inc	hl
	ld	(hl),e	;RSP
	inc	hl
	ld	(hl),d
	ld	hl,6
	ld	e,h	;0		;00000006 - location of start address. - save a byte
	call	ZXPKWD		;PC high - set up PC for all versions. All literal except v6 is packed.
IFDEF	V6routines
	ld	a,(zver)
	cp	6
;	jr	z,v6_start
	jp	z,call1s
ENDIF
;	ld	(zpc),bc
	ld	h,b
	ld	l,c
;	ld	de,0	;e is already zero.
;	ld	(zpc+2),bc
	exx
;zi	scf
	ret
;IFDEF	V6routines
;v6_start:
;	ld	a,1
;	ld	(v_argc),a
;	ld	h,b
;	ld	l,c
;	ld	(v_arg1),hl	;store packed address as routine start for main
;	ld	h,e
;	ld	l,e
;	exx
;	jp	call1s	;call_fv;call routine main to start - set call type to discard result
;ENDIF
;
mkframe:
; Preserves BC, A
; returns HL = new ZSP
; Uses DE
	ld	hl,(zsp)
	ld	de,-38		;Frame size
;	ld	de,-37		;Frame size testing
	add	hl,de


	ld	(zsp),hl

	ld	de,0000	;(rstop)	;Call stack hits routine stack?
rstop:	equ	$-2	;Top of routine stack

	sbc	hl,de		;carry flag set - can be off by one to test high stack overflow

;	ld	hl,(zsp)	;return with the new value
	ld	hl,0000		;return with the new value
zsp:	equ	$-2		;update inline
	ret	nc		;no overflow, return
	jp	spfail1		;stack overflowed! (pop 1 off stack and then fail)

;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
zinst:
	exx
;	ld	hl,(zpc)
	ld	(zipc),hl	;Last opcode location - for debug purposes
;	ld	de,(zpc+2)	;20
;	ld	(zipc+2),de	;20
				;Faster loop if we set up EHL = ZPC and manually fetch instruction
	ld	a,e		;4
	ld	(zipc+2),a	;13
	call	ZXPEEKi
;-R7	ld	d,a		;R7 +4
;-R7	ld	(zipc+2),de	;R7 +20. total 24 vs 30 by moving inst location - auto increment should be positive
;-R7	inc	l	;1 4
;-R7	jp	nz,zinst1	;2 10
;-R7	inc	h	;1 4	
;-R7	jr	nz,zinst1	;2 12/7
;-R7	inc	e	
;-R7zinst1:	
; re-implemented instruction decode as a shallow decision tree
; checking individual exceptions where relevant
	ld	(inst),a	;13
;	cp	0C0h
;	jp	nc,var_op2_group	;instruction is var or 2op

	cp	0B0h
	jp	nc,op0_var_op2		;var/2op/0op

;R	or	a
	add	a,a
;	jp	m,op0or1_inst	;is a>=80h (i.e. top bit set)
;R	jp	m,op1_inst
	jp	c,op1_inst
;
;It's a 2OP.
; replacing each case with JP vs JR for speed.
op2_low:
;;	call	parse_2op	;This decodes the parameters, updates zpc. Preserves A=inst

parse_2op:
;R3	call	zxpkwi		;a and IX unaffected
;R	cp	40h		;we know bit 7 is 0. saves 1 t-state
;R3	or	a		;we've shifted left already so checking bit 7 was bit 6 of inst
;R	jp	c,parse_2op_byte	;bit 6 is 0
	jp	p,parse_2op_byte	;bit 6 of inst is 0
	call	zxpkwi		;a and IX unaffected
;R	cp	60h	;bit 7 is 0, bit 6 is 1 so this tests bit 5
	cp	60h <<1	;shifted above by 1 bit.
	ld	d,a	;common code that doesn't affect flags
	ld	a,b	
	exx	;switch out ZPC
	jp	c,parse_2op_vb
parse_2op_vv:
;;	ld	d,a
;;	ld	a,b	
;;	exx	;switch out ZPC
	call	get_var	;destroys A,DE. Returns HL
	ld	(v_arg1),hl	;16 - 3 bytes
	exx	;switch in ZPC
	ld	a,c
op2_low_var:
	exx	;switch out ZPC
	call	get_var	;destroys A,DE. Returns HL
	ld	(v_arg2),hl	;16 - 3 bytes
	exx	;switch in ZPC
op2_low_ad:
	ld	a,d
op2_low_ret:
	exx			;Switch out ZPC - cheaper to do this after!	
	ld	hl,v_argc	;10
	ld	(hl),2		;10
;R	and	1Fh
	and	1Fh << 1
	ld	hl,two_ops
;R	jp	dispatch
	jp	dispatchX

var_op2_group:
;R	cp	0E0h
	cp	0C0h	; E0h << 1 AND FFh
	jp	nc,var_inst
op2_vainst:
	call	parse_var_getx	;Fetch byte and decode args. 2OP with VAR parameters
	ld	a,(inst)
;R	and	1Fh
	add	a,a
	and	1Fh << 1
	ld	hl,two_ops
	jp	dispatchX
;
;op0or1_inst:
;	cp	0B0h
;	jp	nc,op0_inst
op1_inst:
;	call	parse_1op
parse_1op:
;R	cp	90h
	cp	20h	;90h << 1 AND ffh
	jp	c,p_word_1op
;R	cp	0A0h
	ld	d,a	;preserve A so can now destructively test
	add	a,a	;test bit 5 which is now bit 7!
	jp	M,p_var2_1op	;bit 5 is set
;R4	cp	40h	;0a0h << 1 and ffh
;R4	jp	nc,p_var2_1op
;R4	ld	d,a
	call	ZXPEEKi
;b	ld	(v_arg1),a
;b	xor	a
;b	ld	(v_arg1+1),a
	ld	c,a	;theoretically possible? only impacts JUMP instruction??? - completed below
	exx		;temporary swapout - (8) faster than push/pop! (21)
	ld	c,a
;	ld	b,0
	xor	a
	ld	b,a
	exx		;4
;parse_1op_inc:
;	ld	c,a	;theoretically possible? only impacts JUMP instruction???
;	ld	b,0
	ld	b,a
	ld	a,d
;	inc	l
;	jp	nz,parse_1op_ret
;	inc	h
;	jr	nz,parse_1op_ret
;	inc	e
parse_1op_ret:	
	exx			;Switch out ZPC - cheaper to do this after!	
;	ld	hl,v_argc	; Set argument count correctly - now only done for call routines
;	ld	(hl),1
parse_1op_ret2:	
;R	and	0Fh
	and	0Fh << 1
	ld	hl,one_ops
;R	jp	dispatch
;R5	jp	dispatchX
	add	a,l	;4
	ld	l,a	;4
	ld	e,(hl)	;7
	inc	l	;4 - it's within the page so don't need to roll into h
	ld	d,(hl)	;7
	ex	de,hl	;4
	jp	(hl)	;4	20+4+28=56
;
op0_var_op2:
;R2	cp	0C0h
;R2	jp	nc,var_op2_group	;instruction is var or 2op
	add	a,a
	jp	M,var_op2_group	;instruction is var or 2op
op0_inst:
;	cp	0BEh			;7

;	jp	z,ext_inst		;10	17 saved however, adds 56 (dispatch)+16+17 per ext.
;ext_inst_fix:	EQU	$-2		;Aren't allowed extended opcodes in v1-v4, replaces with fail in loader.
	exx		;Switch out ZPC
;	ld	hl,v_argc
;	ld	(hl),0		;Set argument count correctly - not needed for zero ops.
;R2	and	0Fh
	and	0Fh << 1
;R2	add	a,a
;	ld	hl,zero_ops
	ld	l,a
	ld	h,zero_ops >> 8
;R6	jp	dispatch2
	ld	e,(hl)	;7
;	inc	hl	;6
	inc	l	;4 - it's within the page so don't need to roll into h
	ld	d,(hl)	;7
	ex	de,hl	;4
	jp	(hl)	;4	20+4+28=56
;
ext_inst:
	exx			;Switch in ZPC
	call	zxpkwi
	ld	a,b
	ld	(inst+1),a
	ld	a,c
;	call	parse_var_getx	;Fetch operand types byte and decode args. Count in argc, args in arg1-arg4
	call	parse_var0	;Fetch operand types byte and decode args. Count in argc, args in arg1-arg4
	ld	a,(inst+1)
;R	or	a
;R	jp	m,ext_high	;if bit 7 is set
	add	a,a
	jp	c,ext_high
	ld	hl,ext_ops
	cp	(MAXEXT+1) << 1		;In range?
	jp	c,dispatchX
;zi	scf			;14.2.1 xtended opcodes in the range EXT:29 to EXT:255 should be simply ignored
	ret			; (perhaps with a warning message somewhere off-screen).

;v2_inst:
;	call	ZXPKWI
;	call	parse_v2
;	jp	var_main
;
var_inst:
;	cp	0FAh			;handle exceptions
;	jr	z,v2_inst		;8-operand VAR
;	cp	0ECh
;	jr	z,v2_inst
;	call	parse_var_get	;Fetch operand type and decode. Count in argc, args in arg1-arg4
;var_main:
;	ld	a,(inst)
	exx
;R	and	1Fh
	and	1Fh << 1
	ld	hl,var_ops
;R dispatch:
;R	add	a,a	;4
dispatchX:
	add	a,l	;4
	ld	l,a	;4
;	jr	nc,dispatch2	;12/7
;	inc	h	;4	total = 20/19
dispatch2:
	ld	e,(hl)	;7
;	inc	hl	;6
	inc	l	;4 - it's within the page so don't need to roll into h
	ld	d,(hl)	;7
	ex	de,hl	;4
	jp	(hl)	;4	20+4+28=56
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Other source files
;
	include vmdebug.zsm	;Debugging ops
	include vmvar.zsm	;Access to variables & operand decoding
	include	vm0ops.zsm	;0OP: operations
	include vm1ops.zsm	;1OP: operations
	include vm2ops.zsm	;2OP: operations
	include	vmvops.zsm	;VAR: operations
	include vmeops.zsm	;EXT: operations
	include vmarith.zsm	;Arithmetic operations
	include vmobj.zsm	;Object operations
	include vmprop.zsm	;Property operations
	include vmzchar.zsm	;I/O and buffering
	include vmdict.zsm	;Dictionary oprations
	include vmzz1.zsm	;executed once only so
				;can be overwritten
;	include in_wrhex.inc	;Output hex and decimal numbers

	end
