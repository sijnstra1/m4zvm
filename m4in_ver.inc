;    M4ZVM: Z-Code interpreter for the Z80 processor - expanded and rewritten
;    Copyright (C) 2021-2025 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on:
;    ZXZVM: Z-Code interpreter for the Z80 processor
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; 2018-03-08, Garry Lancaster: Updated version to v1.13
; 2019-05-03, Garry Lancaster: Updated version to v1.14
; 2019-05-17, Garry Lancaster: Updated version to v1.15
; 2021-06-13, Garry Lancaster: Updated version to v1.16
; 2021-10-10 - Shawn Sijnstra porting to TRS-80 Model 4 Release 1 copmlete
; 2021-10-16 - Shawn Sijnstra release 2 - disk buffer and cosmetic fixes
; 2021-10-24 - Shawn Sijnstra release 3 - see Changelog - optimisations + bugfixes
; 2021-10-29 - Shawn Sijnstra release 4 - see Changelog - optimisations + bugfixes
; 2021-11-XX - Shawn Sijnstra - multiple optimisations
; 2021-11-28 - Shawn Sijnstra release 6 - see Changelog - z1/z2 support
; 2021-12-05 - Shawn Sijnstra release 7 - z7 support, extended character support + optimisations
; 2021-12-12 - Shawn Sijnstra release 8 - support for Tandy bit, optimisations
; 2021-12-XX - Shawn Sijnstra release 9 - redoing stack, PRNG, many optimisations, support for
;				 Seastalker sonarscope, fixed use case for no punctuation dictionary search,
;				 fixed $verify regression, improved keymapping
; 2022-01-XX - Shawn Sijnstra release 10 - cosmetic fix for screen scroll, multiple minor optimisations
;					fixed debug local variables display, fixed error handling of disk full during save
; 2022-01-XX - Shawn Sijnstra release 10b - bugfix on >128k support, further code and size optimisation
; 2022-02-XX - Shawn Sijnstra release 10c - core optimisations, z3 status bar speedup
; 2022-02-XX - Shawn Sijnstra Release 10d - cache, branch, ZPC and other core optimisations
; 2022-02-XX - Shawn Sijnstra Release 10e - flush_buf changes, expanded z3 status_update to 80 col
;								bugfix (including zxzvm) correcting unordered dictionary length
;								cosmetic issue - only display CR if that's returned on input
;								(Beyond Zork + Border Zone) as well as other cosmetic regression
;											  for long input lines
; Release 11 - minor speedup on input line conversion + regression fix
;				reverse text colour select enabled as z6 games ignore colour not available flag
;				and cosmetic fix to Shogun opening screen
; Release 11a - fix font change to not remove highlighting
; Release 11b - flush_buffer fix on white background
;				speedups to NEGrr, char print, fix to convert sentence space to space (Shogun)
;				Rewrote @art_shift/@log_shift
; Release 11c - rearranged switches to have -s for silent, -t for tandy bit, -ts or -st for both
;				improved file error handling (added ABORT instruction)
; Pre-12	- removed unused space, tidied restart code including z3 cosmetic issue
; Release 12 - general cleanup including making source more Model 1/3 cross platform
;			and minor cache increase due to space savings (20->21K)
; Release 13 - improved save and minor speed tweaks
; 		and reworking memory map to make create integrated 64K version
; 		tweaks made to the screen scroll for BeyondZork cosmetic issues
;		speed improvement to core (zinst + dispatch) + scan_table (word), 1ops
;		backward compatible (ignore LRL error), input regression fixes, fixed: 1ops
;		inclcl,declcl,use built-in DOS error messages
;
; Release 14 - 2022-05-08 - Shawn Sijnstra
;		Core rewrite for Speed improvements from Garry Lancaster (keep ZPC in E'H'L'),
; 		fixed zpoke speed for 64K, streamlined 128k memory fetch, adjusted memory map for more
; 		cache in 64K mode, rewrote encode routine, fixed a bug when turning on "stream 0",
;		rewrote call and return code to improve speed, rewrote branch code,
;		rewrote cache trawl routine to use registers for tracking
;		synchronised VM code base across model 1/3/4 versions (model 2 WIP)
;		tweaked timed input, instruction decode + 1op + 2op decode
; Release 15 - 2022-08-07 - Shawn Sijnstra
;		Changes to parsing var/1op/2op and into dispatch
;		Tweaks to buffered text out to improve display speed
;		Tweaks to cursor queries including wrap check
;		Redo stack, buffers + other changes -increase cache by 3 sectors [64K only]
;		Multiple changes to cache routines for speed improvement
;		Tweaks to object, attribute and property routine efficiency
;		Change to core tracking of exceptions/error handling
;		Fixed printing to properly honour transcript bit + speed improvements
;		Rewrites to flush_buf, stream 2 and stream 3 for speed improvement
;		Tweaks to z6 routine efficiency
; Release 16 - 2023-07-16 - Shawn Sijnstra - Release 16
;		Cache streamlining, ipeekw/peekw optimisation, dispatch tweak, pokew tweaks (128k)
;		Tweaks to core stream 3 and charout; tweaks to window select, changed build script
;		rewritten dispatch routine + update to debug tracking
; Release 17 - 2024-01-02 - Shawn Sijnstra
;		Reshuffling the 64K memory map to allocate more space to disk cache, less to
;		z-stack. Minor tidying of messages.
;		16b - speed tweaks to call1s (-2T), tweak to return (HL=0 only), v3jin
;		16c - reduce file size converting zeroes to uninitialised space,
;				reduce debug footprint, reduce space in alphabet, minor speed tweak on
;				objects. Tweaks to res_more and swnd
;				bugfix - pre-initialised color scheme on 64K versions (impacted Shogun)
;				bugfix - some error messages not passed through
;		16d -	bugfix - edge case on disk full wasn't handled properly
;				removed support for old saved game versions, rewrote fixup_stack
;				rewrote frame dump to be smaller, neater and reusable
;				moved more content into burnable area
;				added two more sectors of cache in 64K version
; 17a changes
;		changes to stream handling to speed up vmzchar
;		remove double-checking z version (monolithic build)
; 18 changes
;		redo core so that all ZXPEEK calls auto-increment. 2% speed improvement except Arthur
;		added extra sector cache, redid @copy_table backwards & scan_byte to be slightly faster.
;		removed surplus byte in z6 move_window, tweaked zxscur (cursor move)
;		tweaks to init routine, bwdcpy speed
;		optimised initial startup code 
;		bytes save in vm0ops status line
;		moved era_all to include zxcls and adapt for initialize
;		optimized fwdcopy, tokenize, inibuf (zvm side)
;		major rewrite find_word, next_token
;		added another sector (saved another block)
;		Cosmetic bug - handle auto wrapped line followed by immediate line feed
;		 - ignore solo linefeed in this case.
;		changes to inibuf to use burnable memory space.
;		rearranged memory map in core VM to reduce load footprint.
;		change to formula for cache entries.
;TODO: move io_zvbad? - tweak llf code
;TODO: move io_zvbad?
;TODO: change the input from ZXpk64RW to increment
;Done: Try using printwrd with compressed strings for opening banner. - seems to be very inefficient

	defb	'M4ZVM Rel 18 Serial 250209'

