;    Vezza: Z-Code interpreter for the Z80 processor
;    Z1/2/6/7 Extensions, bugfixes, rewrites and optimisations
;    Copyright (c) 2021-2025 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on original ZXZVM:
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;    Portions copyright 2019 Garry Lancaster
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
ctype:	defb	0	;Call type of current call operation (0/1/2/3)
			;0 => CALL_VS (store return value)
			;1 => CALL_VN (discard return value)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;var_ops:	;VAR:0         1         2      3        4       5      6    7
;	defw	call_vs, zstorew, zstoreb,zpprop,  zread0, print_char, prnum,z_rand
;	defw	zput,    zpull,   split_w, sel_w,call_vs2, erase_w,erase_l,setcur
;	defw	getcpos, style,   sbfmde,  ostrm,  istrm, sfx, read_char, scantbl
;	defw	znot0,    call_vn, call_vn2, ztok, zencode, cpytab, prtbl,ck_arg
; zpull is replaced by zpull_v6 at load if required.
; setcur is replaces with setcur_v6 at load if required.
;
call_vn2:
	call	parse_v2
	ld	c,1
	jp	call_genc
call_vn:
	call	parse_var_get
;call1n:
call2n:

;	ld	a,1	  ; Call type 1
	ld	c,1	  ; Call type 1
	jp	call_genc 	;jp is faster than jr when not conditional.

call_vs2:
	call	parse_v2
	jp	call2s
call_vs:
	call	parse_var_get
;call1s:
call2s:

;	xor	a
	ld	c,0
;Entry: A is call type	
;call_gen:
;	ld	(ctype),a	;13 Set call type
;	ld	c,a		;4 preserve in D for now
;Entry: C is call type
call_genc:
	ld	hl,(v_arg1)	;Packed routine address
call_genhl:			;HL has packed address and C has call type
	ld	a,h
	or	l		;<< v0.04 Return false if HL = 0
	jp	nz,call_g1	;split for versions - jump replaced for v1-4 (call_g1_34)
				;so register use needs to match
call_fix:	equ	$-2	;<< v1.12 Don't return anything if we are
				;   discarding the result
;	ld	a,(ctype)	;13
;;	ld	a,c
;;	or	a
	or	c		;A is already 0 from above - New 31-Oct-2023 -4

	jp	z,ret_hl	;   Type 0: Discard result (ret_hl does SCF)
;zi	scf
	ret			;   Type 1: Just return
				;>> v1.12

call_g1:			;>> v0.04
upack_addr_R_O_5:
	call	0		;replaced with upack_addr_R_O at init.	;Unpack routine address, preserves A, v6/7 use DE
;	call	ipeek
;ez	call	ZXPEEK		;preserves D
	call	ZXPEEKi		;preserves D
	ld	b,a		;B = no. of local variables
;;	ld	a,c
;;	ex	AF,AF'		;A' now has ctype
;ez	inc	l		;speedup
;ez	jp	nz,call_g2
;ez	inc	h
;ez	jr	nz,call_g2
;ez	inc	e
call_g2:	
;;	ld	a,(v_argc)
;;	dec	a
;;	ld	c,a		;C = no. of parameters
;;	cp	b		;If C <= B, ok
;;	jr	c,cfv1
;;	ld	c,b		;Ignore surplus parameters
;;cfv1:
;	push	hl
;	push	de		;EHL -> start of routine
;;	push	bc		;stright into the prime registers above
;;	EXX
;;	pop	bc
;x        push    de              ;save start of: v5+:routine; v1-4:default values
;x        push    hl		;11

	ld	a,e	;4
	push	hl	;11	= 15 vs 22 

	call	mkframe		;HL has new frame; Alt registers + BC A preserved
        exx                     ;switch in ZPC
;x       push    hl	;11
;x       ld      a,e	;4

	ex	(sp),hl	;19	load in HL/push old from ZPC
	ld	d,a		;swap old and new E
	ld	a,e
	ld	e,d		; = 31 vs 15
; totals = 0 + 15 + 31 vs 28 + 22 + 15. Saves 19!! - now saves 19.
        exx                     ;switch out ZPC
        pop     de		;ADE now has old ZPC

;	ld	de,(zpc)	;20
	ld	(hl),e		;7
	inc	hl		;6
	ld	(hl),d		;7
	inc	hl		;6
;	ld	de,(zpc+2)	;Old ZPC into frame
;	ld	a,(zpc+2)	;13 only need this byte for EHL record
	ld	(hl),a		;7
	inc	hl		;6 total = 4 x 13 + 20 = 72

	inc	hl
;	ld	a,(ctype)	;13 Procedure, function or interrupt? (total = 2*13)
;;	ex	AF,AF'		;4 Procedure, function or interrupt? (from above - total = 4*4 = 16)
;;	ld	(hl),a
	ld	(hl),c		;C has call type
	
	ld	a,(v_argc)
	dec	a
	ld	c,a		;C = no. of parameters
	cp	b		;If C <= B, ok
	jr	c,cfv1
	ld	c,b		;Ignore surplus parameters
cfv1:

	inc	hl
	ld	(hl),c		;;No. of parameters
	inc	hl
	ld	de,(rsp)	;Routine stack pointer
	ld	(hl),e
	inc	hl	
	ld	(hl),d
	inc	hl
	ld	a,c
	or	a
	jr	nz,argmov
	ld	a,b
	sub	c
	ld	b,0
	jp	argzero	;z flag is already set now

argmov:
	ld	de,v_arg2
arglp:
	ld	a,b
	sub	c	;a= b-c (how many will need to be zeroed below)
	ex	de,hl
	ld	b,0
	sla	c	;c=c*2 (2 bytes per arg)
	ldir		;transfer supplied args
	ex	de,hl
	or	a
argzero:		;zero any additional args
;x	jr	z,noarg	;none need to be zeroed
;zi	scf		;will flow through the loop
	ret	z	;none need to be zeroed
arglp2:	
	ld	(hl),b
	inc	hl
	ld	(hl),b
	inc	hl
	dec	a	;doesn't touch carry flag
	jr	nz,arglp2

noarg:
;x        exx                     ;switch in ZPC
;x        pop     hl              ;set new ZPC
;x        pop     de
;x        exx                     ;switch out ZPC
; = 0 vs 28

;	exx
;	ld	a,e
;	ld	(zpc+2),a
;	pop	hl
;	ld	(zpc+2),hl
;	pop	hl	
;	ld	(zpc),hl	;New ZPC
;x	scf
	ret


; Call routine for versions 3-4
call_g1_34:
upack_addr_R_O_34:
        call    0		;Updated to correct upack_addr_R_O at init.
        		     ;to unpack routine address
;	call	ipeek
;ez	call	ZXPEEK		;preserves D
	call	ZXPEEKi		;preserves D
;	EX	AF,AF'
	ld	b,a		;B = no. of local variables
;;	ld	a,c
;;	EX	AF,AF'
;ez	inc	l
;ez	jp	nz,call_g2_v34
;ez	inc	h
;ez	jr	nz,call_g2_v34
;ez	inc	e
call_g2_v34:	
;	EX	AF,AF'
;	ld	b,a		;B = no. of local variables
;x        push    de              ;save start of: v5+:routine; v1-4:default values
;x        push    hl		;11

	ld	a,e	;4
	push	hl	;11	= 15 vs 22 

;	push	de		;EHL -> start of routine
;	ld	a,d		;= 12 + 13 = 25 t-states to swap order vs 10+11+20 = 41.
	call	mkframe		;HL has new frame. Preserves A.
	exx
;x       push    hl	;11
;x       ld      a,e	;4

	ex	(sp),hl	;19	load in HL/push old from ZPC
	ld	d,a
	ld	a,e
	ld	e,d		; = 31 vs 15
; totals = 0 + 15 + 31 vs 28 + 22 + 15. Saves 19!!
        exx                     ;switch out ZPC
        pop     de		;ADE now has old ZPC


	ld	(hl),e		;7
	inc	hl		;6
	ld	(hl),d		;7
	inc	hl		;6
;;	ld	a,(zpc+2)	;Old ZPC into frame
;	ld	de,(zpc+2)
	ld	(hl),a		;preserved from above
	inc	hl	
	inc	hl
;	ld	a,(ctype)	;Procedure, function or interrupt?
;	EX	af,af'		;restore ctype
;	ld	(hl),a
	ld	(hl),c		; Call type - Procedure, function or interrupt?
	
	ld	a,(v_argc)
	dec	a
	ld	c,a		;C = no. of parameters
	cp	b		;If C <= B, ok
	jr	c,cfv1_34
	ld	c,b		;Ignore surplus parameters
cfv1_34:


	inc	hl	
	ld	(hl),c		;;No. of parameters
	inc	hl
	ld	de,(rsp)	;Routine stack pointer
	ld	(hl),e
	inc	hl	
	ld	(hl),d
	inc	hl

	ld	a,c		
	or	a
	ld	a,b		;preserve b (number local vars)
;y	push	bc

;	ex	de,hl
	jr	z,noarg_34		;No args to transfer in to frame
	push	bc
	ex	de,hl
	ld	hl,v_arg2	;parameters that override default

	ld	b,0
	sla	c	;c=c*2 (2 bytes per arg)
	ldir
	ex	de,hl
	pop	bc
noarg_34:
;y	pop	bc
;x        exx                     ;switch in ZPC
;x        pop     hl              ;set new ZPC
;x        pop     de
;x        exx                     ;switch out ZPC

	or	a	;number of local vars
;zi	scf
	ret	z

;;	ld	hl,(zsp)
;;	ld	de,4+4		;adjusted for new format.
;;	add	hl,de		;HL->local vars


;	ex	de,hl		;HL now has the current location within the frame
				;including adding in however many args copied.

;	ld	d,a		;loop of how many local vars left to update with default now kept in D

;;	ld	a,(v_argc)	;A = no. of parameters (which override the
;;				;initial values), left off by one to make the loop work
	ld	a,c
	add	a,a		;words to skip over
	exx
	add	a,l
	ld	l,a
	jr	nc,argl0
	inc	h
	jr	nz,argl0
	inc	e
argl0:
	exx
	ld	a,b	;if locals - parameters = 0 then we are done.
	sub	c
;zi	scf
	ret	z
;	ld	d,a		;loop of how many local vars left to update with default now kept in D
argl:
;	dec	a
;	jr	nz,argl3	;Argument was provided, so skip using the default values

;	push	hl
	exx
;	ld	hl,(zpc)
;	ld	de,(zpc+2)	;not needed - e is preserved.
	call	ZXPKWI		;Get default parameter into BC. Routine preserves A
;	ld	(zpc),hl	;cheapest way to save HL. E is already preserved and updated later.
;	ld	(zpc+2),de
	push	bc
	exx
	pop	bc	
;	pop	hl

;;	dec	a
;;	jr	nz,argl2	;Argument was provided, so skip using the default
;
argl1:
;	inc	a		;To counterbalance the dec a above, so that
	ld	(hl),c		;the zero test works the next time round
	inc	hl		;Copy in the initial values, flipping them
	ld	(hl),b		;to little-endian as we go
	inc	hl
;argl2:
;	dec	d		;total local var count
	dec	a		;local vars to update
	jr	nz,argl
;zi	scf
	ret			;>> v0.04


;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; v5+ @check_arg_count argument-number
ck_arg:
	call	parse_var_get
	ld	ix,(zsp)	;20T/4bytes
	ld	a,(v_arg1)	;Argument number to check
	cp	(ix+5)		;19T/3bytes No. of arguments
;	cp	(ix+4)		;No. of arguments = parameters in zframe
	jp	z,branch
	jp	c,branch	;If A <= no. of args, it is provided.
	jp	nbranch		;Not provided.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Push and pop...
; @push
zput:
	call	parse_var_get
;	ld	hl,(v_arg1)
	ld	bc,(v_arg1)
	jp	zpush	;it has scf and ret
;	scf
;	ret
;
; Note that the return method for pull is different for v6
; @pull (variable)
zpull:
	call	parse_var_get
;	call	zpop		;HL = value
	ld	hl,(rsp)
;	ld	e,(hl)
	ld	c,(hl)
	inc	hl
;	ld	d,(hl)
	ld	b,(hl)
	inc	hl
	ld	(rsp),hl
;	ex	de,hl

	ld	a,(v_arg1)	;A = variable number
;	scf
	jp	put_var_ind
; V6 @pull [stack] -> (result)
IFDEF	V6routines
zpull_v6:
	call	parse_var_get
	ld	a,(v_argc)
;	cp	1		;is there an argument?
;	jr	nc,zpull_v6_2	;yes - user stack
	or	a		;is there an argument?
	jr	nz,zpull_v6_2	;yes - user stack
	ld	hl,(rsp)	;no - system stack
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	(rsp),hl
	jp	ret_bc

zpull_v6_2:
	ld	hl,(v_arg1)
	ld	e,0		;dynamic mem so e is always 0
	call	ZXPKWD
	inc	bc
	call	zxpokw	;store the new user stack space size
	add	hl,bc
	add	hl,bc
	call	ZXPKWD
	jp	ret_bc
ENDIF
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Historical notes:
; checks for overwrite of 0011h
; Certainly happens in minizork and Zork I. Circumvents Stream 2 initialisation
;
;Note:
;1.1.1.1
;By tradition, the first 64 bytes are known as the "header".
; The contents of this are given later but note that games are not permitted
; to alter many bits inside it.
;Only bits 0,1,2 of the Flags 2 word may be legally changed by the game
;Flags 2:
;z1+	*	*	*	Bit 0: Set when transcripting is on
;z3+	*		*	1: Game sets to force printing in fixed-pitch font
;z6	*	*		2: Int sets to request screen redraw: game clears when it complies with this.
;this means the only time flush should even be done might be on z6 when refresh is cleared?
;or transcript bit directly impacted. This approach is expensive.
;Testing removal of this catch for speed and efficiency.

zstorew:
	call	parse_var_get
	ld	hl,(v_arg1)
	ld	de,(v_arg2)
	add	hl,de
	add	hl,de
	ld	bc,(v_arg3)
	ld	a,h		;page 0?
	or	a
	jp	nz,zxpokw	;above 00FFH so just do it
	ld	a,l
	cp	12h		;in the header near the transcript?
	jp	nc,zxpokw	;no, just do it
;	jp	zxpokw
;zstorew_slow:
;;	ld	a,b
;	ld	d,b
;	push	bc
;	call	trap_poke3	; << v0.03 >> trap writes to location 11h
;	pop	bc
;	inc	hl
;;	ld	a,c
;	ld	d,c
;	jp	trap_poke3	; << v0.03 >> trap writes to location 11h
; this version saves a byte and assumes that the rest of the header is unwritable.
	push	hl
	push	bc
	call	flush_buf
	pop	bc
	pop	hl
	jp	zxpokw

zstoreb:
	call	parse_var_get
	ld	hl,(v_arg1)
	ld	de,(v_arg2)
	add	hl,de
;	ld	a,(v_arg3)
;
;<< v0.03 If there's a write to Flags 2 (at address 11h) then flush buffers first
; destroys: D
;trap_poke:
;	ld	d,a
	ld	a,h
	or	a
;	ld	a,d
	ld	a,(v_arg3)

	jp	nz,zxpoke	;above 00FFH, so doesn't need trapping.
	ld	d,a
;trap_poke3:
;;	ld	d,a
	ld	a,l
	cp	11h
	ld	a,d	;restore a
;;	jp	nz,trap1
	jp	nz,zxpoke
	push	hl
	push	af		;added push/pop as we no longer preserve in flush_buf
;;	push	bc		;added push/pop as we no longer preserve in flush_buf
	call	flush_buf	;no longer preserves all registers
;;;	call	z,flush_buf	;preserves all registers
;;	call	ZXUSCR		;left here for historical purposes - seems unused in context.
;;	pop	bc
	pop	af
	pop	hl
;trap1:
;	ld	a,d	;restore a
	JP	ZXPOKE ;this ALWAYS sets the carry before return anyway. Speed improvement.
;
; >> v0.03
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
zpprop:
	call	parse_var_get
	ld	bc,(v_arg1)	;DE = object no.
	ld	a,b		;<< v1.01
	or	c
;zi	scf
	ret	z		;>> v1.01 Object 0 errors
;	ld	bc,(v_arg2)	;C  = property no.
;;	ld	hl,(v_arg3)	;HL = value
;	jp	putprop

;Set object DE property C to HL
;
putprop:
		;store_property()
;	push	hl	; grab directly from v_arg3 later
        call    propadd ;HL = Address of property list
;       	ld	bc,(v_arg2)	;C  = property no.
       	ld	A,(v_arg2)	;A  = property no.
       	ld	d,a
IFNDEF	ObjError
	inc	d
ENDIF
ppr3:   call    ZXPK64rw  ;Get property ID
pnmask_fix5:
	and	03fh
        cp      d
IFDEF	ObjError
        jr      z,ppr4 ;Found! - move to check found before overflow
ENDIF
;        jp	c,proper1	;property error - never seen this happen.

	jr	c,ppr3nf
        call    propnxt ;Next property
pnv3fix_5	EQU	$-2
        jp      ppr3
;
ppr3nf:
IFDEF	ObjError
;	call	ilprint
;	defb	13,'Warning: Property not found!$'
	call	PNFerr
ENDIF
ppr4:
        call    ZXPK64rw
        inc     hl	;faster
psmask_fix2:
	and	03fh
        jr      nz,wword
        ld	a,(v_arg3)
;	pop	de
;	ld	a,e
	JP	ZXPOKE ;this ALWAYS sets the carry before return anyway. Speed improvement.
wword: 	
;	pop	bc
	ld	bc,(v_arg3)	;20 vs 26 t-states
;	call	zxpokw
	jp	zxpokw
;;	inc	hl		;unnecessary
;	scf
;	ret

;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Screen operations. These mostly map directly to ZXIO functions
; @erase_window window
erase_w:
	call	parse_var_get
	call	flush_buf
	ld	a,(v_arg1)
	jp	ZXERAW
; @erase_line value
erase_l:
	call	parse_var_get
	ld	a,(v_arg1)
	dec	a
;zi	scf
	ret	nz
	call	flush_buf
	jp	ZXERAL
;
; @split_window lines
split_w:
	call	parse_var_get
	call	flush_buf
	ld	a,(v_arg1)
	jp	ZXSWND

split_w3:			;v3 written to support seastalker
	call	parse_var_get
IFDEF	v3Split
	call	flush_buf
	ld	a,(statusbar)
	ld	b,a
	ld	a,(v_arg1)
	add	a,b
	or	a
	jp	z,zxswnd
	call	zxswnd
	ld	a,(statusbar)	;need to check if this selects the upper window?
	or	a
	call	nz,sstatusbar	;resets to bottom window
	ld	a,1
	call	ZXUWND		;do we need to select upper window to move cursor?

	ld	bc,0101h	;need to select window
	ld	a,(statusbar)
	add	a,b
	ld	b,a
	call	ZXSCUR
	ld	a,0
	jp	ZXUWND		;select upper window/move cursor
ELSE
;zi	scf
	ret
ENDIF
;
;  @set_window window
sel_w:
	call	parse_var_get
	call	flush_buf
	ld	a,0
	call	ZXSTYL		;turn off highlighting when selecting a window. Needed for v6
	ld	a,(v_arg1)
;	cp	5		;v6 workaround - testing as 6 now instead of 5
;	ccf
;	ret	c
	cp	7
	jr	nz,sel_wskip
	ld	a,0
sel_wskip:
	and	1
	xor	1	
	ld	(cwin),a
	xor	1
	jp	ZXUWND

sel_w3:
	call	parse_var_get
	ld	a,(statusbar)	;seastalker
	or	a
	jr	z,sel_w
	call	flush_buf
	ld	a,(v_arg1)
	xor	1
	ld	(cwin),a
	xor	1
	jp	z,ZXUWND
	call	ZXUWND
	ld	BC,0201h
;	call	ZXSCUR
;	scf
;	ret
	jp	zxscur

;
getcpos:
	call	parse_var_get
	call	flush_buf
	call	ZXGETX
	ld	d,l	;D has X
	call	ZXGETY
	ld	b,h	;0	; BC = Y	
	ld	c,l
	ld	hl,(v_arg1)

	call	zxpokw	;Store Y in the array (Preserves BC, DE, HL)
	inc	hl
	inc	hl
	ld	c,d	;BC = X
	jp	zxpokw	;Store X in the array & return
;
style:
	call	parse_var_get
	call	flush_buf
	ld	a,(v_arg1)
	jp	ZXSTYL
; @set_cursor
setcur:
	call	parse_var_get
	call	flush_buf	;shouldn't be possible to set cursor position and not be top window
	ld	a,(v_arg1)
	ld	b,a
	ld	a,(v_arg2)
	ld	c,a
	jp	ZXSCUR
;
; v6 version
; @set_cursor line column window
IFDEF	V6routines
setcur_v6:
	call	parse_var_get
	call	flush_buf
	ld	a,(v_arg1)
	ld	b,a
	ld	hl,(v_arg2)
	ld	c,l
;	bit	7,b
	ld	a,b
	cp	252		;to allow -1, -2 for cursor on off switches
	jp	nc,ZXSCV6	;it's a cursor switch command
;	and	3fH		;strip top bits as a workaround for Mysterious Adventures?
;				; doesn't seem to work.
;	ld	b,a
;	ld	a,h
;	cp	255		;negative?
;	inc	a		;
	bit	7,h		;is hl negative?
	jr	z,setcur_v6c
	ld	de,screen_w-1		;hard coded screen with -1
	add	hl,de
;	ld	c,l		;done below
	dec	b		;wrapped back to previous line
setcur_v6c:
	ld	de,(setcur_off)
	add	hl,de
	ld	c,l
	ld	a,(v_argc)
	cp	3
	jp	c,ZXSCUR	;no window specified so behave as normal.
	ld	a,(v_arg3)
;Add extra code to correct for window 7 mapping to window 0
	cp	7
	jr	nz,setcur_v67
	ld	a,0
setcur_v67:
	and	1
	jp	ZXSCV6
ENDIF	
;
print_char:
	call	parse_var_get
prchar:
	ld	hl,(v_arg1)
;zi	call	ll_zchr
;zi	scf
;zi	ret
	jp	ll_zchr
;
sbfmde:
	call	parse_var_get
	ld	a,(v_arg1)	;Set buffer mode
	ld	(bufmde),a
	or	a
;zi	call	z,flush_buf
;zi	scf
	jp	z,flush_buf
	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
prnum:
	call	parse_var_get
	ld	de,numbuf
	ld	hl,(v_arg1)
	bit	7,h	;Negative?
	jr	z,prnum1
	ld	a,'-'
	ld	(de),a
	inc	de
	call	neghl
prnum1:	call	spdec
	ld	a,'$'
	ld	(de),a
	ld	de,numbuf
opbuf:	ld	a,(de)
	cp	'$'
;zi	scf
	ret	z
	ld	l,a
	ld	h,0
	push	de
	call	ll_zchr
	pop	de
	inc	de
	jr	opbuf

numbuf:
	defb	'-00000$'
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Text entry interrupt...
;
tr_a1:	defw	0
tr_a2:	defw	0
tr_a3:	defw	0
tr_a4:	defw	0	;Arguments
;
timer:	defb	0	;In the timer already?
;
rch_timer:
;	ld	c,3	;Timer called from read_char
;	ld	de,(v_arg3)
;	ld	(tr_a3),de
	ld	hl,(v_arg3)
	ld	(tr_a3),hl
	ex	de,hl	;saves 4 t-states and 1 byte
	ld	hl,(v_arg4)
	ld	(tr_a4),hl
	ld	c,a	;preserve a - contains returned charater
	ld	a,(timer)
	or	a
	ld	a,c
	ld	c,3	;timer called from read_char
	ret	nz
	inc	a
	ld	(timer),a
	jp	rch_t1
;
timer_int:
	ld	c,2	;Timer called from read_line
	ld	a,(timer)
	or	a
	ret	nz
	inc	a
	ld	(timer),a
;
;DE = routine, HL = time
;
	ld	(tr_a3),hl
	ld	(tr_a4),de
rch_t1:	ld	hl,(v_arg1)
	ld	(tr_a1),hl
	ld	hl,(v_arg2)
	ld	(tr_a2),hl
;	ld	(v_arg1),de	;Routine address - don't store it to retrieve later
	ld	a,1
	ld	(v_argc),a
	pop	hl		;Return address, we don't use this.
;	ld	a,c
;	jp	call_gen
	ex	de,hl
	jp	call_genhl	;c already has call type, hl has v_arg1
;
rchr_iret:
	ld	hl,(tr_a1)
	ld	(v_arg1),hl
	ld	hl,(tr_a2)
	ld	(v_arg2),hl
	ld	hl,(tr_a3)
	ld	(v_arg3),hl	;=16*6=96
	ld	a,3
	ld	(v_argc),a
;;	ld	a,(timer)	;13
;;	dec	a		;4
;;	ld	(timer),a	;13 = 30 total
	ld	hl,timer	;10
	dec	(hl)		;7
;	ld	a,d
;	or	e
	ld	a,b
	or	c
	jp	z,rchr
	ld	bc,0
	jp	ret_bc
;
timer_iret:
	ld	hl,(tr_a1)	;16
	ld	(v_arg1),hl
	ld	hl,(tr_a2)
	ld	(v_arg2),hl
	ld	hl,(tr_a3)
	ld	(v_arg3),hl
	ld	hl,(tr_a4)
	ld	(v_arg4),hl
	ld	a,4
	ld	(v_argc),a
;	ld	a,(timer)
;	dec	a
;	ld	(timer),a
	ld	hl,timer
	dec	(hl)
;	ld	a,d
;	or	e
	ld	a,b
	or	c
	jr	nz,iterm
;
;The input routine wants the z-program to have printed its data for it again
;after a return from a timer interrupt...
;
	call	flush_buf	; ensure any output is flushed before resuming
;	retained for potential future use
;	call	ZXIRES		; tell I/O module that input is being resumed
	ld	hl,(v_arg1)
	inc	hl
	call	ZXPK64rw	;Length of line - ASSUMPTION - it's in the buffer area
	or	a
	jr	z,zread
	ld	b,a
	ld	a,(zver)
	cp	5
	jr	nc,iplp1
iplp4:	call	ZXPK64rw	;<< v1.10 reprint input line in the v4 model - NEEDS TESTING
	or	a
	jp	z,sreadt	;Reprint the line
	inc	hl
	push	hl
;	push	bc
	ld	l,a
	ld	h,0
	call	char_out
;	pop	bc
	pop	hl
	jp	iplp4	;>> v1.10 end reprint input line

iplp1:	inc	hl	;z5+ model
	call	ZXPK64rw	;Length of line - ASSUMPTION - it's in the buffer area	;Print it
	push	hl
IFDEF	CPM
	push	bc
ENDIF
	ld	l,a
	ld	h,0
	call	char_out
IFDEF	CPM
	pop	bc
ENDIF
	pop	hl
	djnz	iplp1
	jp	zread5	;don't re-check version number
;
;Terminate input at once...
;
iterm:	ld	hl,(v_arg1)	;Input buffer
	call	ZXPK64rw
	ld	b,a		;B = no. of bytes
	inc	b		;+1 for length
	xor	a
ztlp:	inc	hl
	call	ZXPOKE		;Zap the buffer
	djnz	ztlp
	jr	aread2
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Reading text
;
aread00:
	ld	(v_arg3),de	;added for safety
	ld	(v_arg4),de
	jr	aread1
zread0:
	call	parse_var_get

zread:	ld	a,(zver)
	cp	5
	jr	c,sread
zread5:
	call	flush_buf
aread:	ld	hl,(v_arg1)
	ld	a,(v_argc)
	ld	de,0
	cp	3
	jr	c,aread00
	ld	de,(v_arg3)
aread1:	
;;	call	flush_buf	;moved to before the registers are loaded - no longer preserves registers
	call	ZXINP	;Input line - C is the finish character, b is 0, z is timer ran out. Change of regiser use!
;	ld	a,b
;	or	a	;Timeout
	jr	nz,aread2
	ld	hl,(v_arg3)	;Timeout
	ld	a,h
	or	l
	jr	z,aread2
	ld	de,(v_arg4)
	ld	a,d
	or	e
	call	nz,timer_int	;This function only returns if unsuccessful.
aread2:
	push	bc
	ld	a,c		;changed register use.
	cp	0dh		;print the CR if that was the returned character
	jr	nz,aread3
	ld	hl,0dh
	call	ll_zchr
aread3:
;	push	bc		;<< v1.11 >> save result from read op 
	ld	hl,(v_arg1)	;<< v0.04 remove invalid characters
	inc	hl		;Length
	call	ZXPK64rw		;A = actual length
	or	a
	jr	z,areadw	;No length?
	ld	b,a
areadv:	inc	hl
	call	ZXPK64rw		;Character
	call	valid_char
	call	ZXPOKE
	djnz	areadv		;>> v0.04
;areadw:	- need to insert CR if a CR is returned!
;	ld	hl,0dh
;	call	ll_zchr
areadw:
	ld	hl,(v_arg2)
	ld	a,h
	or	l
	jr	z,nopse	;no parsing required
;
;Tokenise?
;
	ld	bc,0
	ld	a,c
	ld	hl,(v_arg1)
	ld	de,(v_arg2)
	call	tokenise

nopse:
;	pop	hl		;Returned keystroke
;	ld	l,h
;	ld	h,0
	pop	bc
;	ld	c,b		;changed register use. BC is returned character now.
;	ld	b,0	
;	jp	ret_hl
	jp	ret_bc
;
;<< v0.04 read for v3
;
;I'm going to do v3 input by converting the input format to v5-style before,
;and back afterwards. There's enough room, because the 1 byte for length
;matches the 1-byte terminator.
;
sreadt:
	call	sstatus		;<< v1.10 rewritten for timer events in v4
	call	line_from_v5
	ld	hl,(v_arg1)
	jr	sreads

sread:	call	sstatus
	call	flush_buf
	ld      hl,(v_arg1)
	inc	hl
	xor	a
	call	ZXPOKE		;Write current length = 0 (no passed data)
	dec	hl
sreads: ld      de,0
	ld	a,(zver)
	cp	4
	jr	c,sreadu
        ld      a,(v_argc)
        ld      de,0
        cp      3
        jr      c,sreadu
        ld      de,(v_arg3)	;>> v1.10

sreadu:	;call    flush_buf	; moved to before the registers are loaded - no longer preserves registers
        call    ZXINP		;Input line

	push	bc		;<< v1.10
	call	line_to_v5
	pop	bc		;>> v1.10

        ld      a,c		;<< v1.10 Handle timeouts in v4 input. Changed register use.
        or      a      		;Timeout
        jr      nz,sreadr
        ld      hl,(v_arg3)     ;Timeout
        ld      a,h
        or      l
        jr      z,sreadr
        ld      de,(v_arg4)
        ld      a,d
        or      e
        call    nz,timer_int    ;This function only returns if unsuccessful.
				;>> v1.10

sreadr:	ld      hl,0dh
        call    ll_zchr
;
; << v1.10 >> v5 -> v4 line converter was here
;
sread1:
	ld      hl,(v_arg2)
        ld      a,h
        or      l
;zi	scf
	ret	z
;
;Tokenise?
;
        push    bc
        ld      bc,0
        ld      a,c
        ex	de,hl	;hl already has v_arg2
        ld      hl,(v_arg1)
;        ld      de,(v_arg2)
        call    tokenise
        pop     bc
;zi	scf
	ret
;>> v0.04
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Read a character
;
read_char:
	call	parse_var_get
rchr:
	call	flush_buf
	ld	a,(v_argc)
	cp	2
	ld	de,0
	jr	c,rchr1
	ld	de,(v_arg2)
rchr1:;	call	flush_buf
	call	ZXRCHR
	or	a
	call	z,rch_timer
	call	valid_char
;	ld	l,a
;	ld	h,0
	ld	c,a
	ld	b,0	
;	jp	ret_hl
	jp	ret_bc
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Output and input streams
;
ostrm:
	call	parse_var_get
	ld	a,(v_arg1)
	ld	hl,(v_arg2)	;where to store stream 3
	jp	ll_strm
;
istrm:		;Instruction has no effect. future expansion
	call	parse_var_get
	ld	a,(v_arg1)
	ld	hl,(v_arg2)
;zi	scf
	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
sfx:
	call	parse_var_get
	ld	hl,(v_arg1)
	ld	de,(v_arg2)
	ld	bc,(v_arg3)
;zi	call	ZXSND
;zi	scf
;zi	ret
	jp	zxsnd
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
z_rand:
	call	parse_var_get
	ld	hl,(v_arg1)
	call	random
	jp	ret_hl
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;@not
znot0:
	call	parse_var_get
;znot:
	ld	hl,(v_arg1)
	ld	a,h
	cpl
	ld	b,a
	ld	a,l
	cpl
	ld	c,a
	jp	ret_bc
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Tokenise & encode text
;
; @tokenise text parse dictionary flag
ztok:
	call	parse_var_get
	ld	hl,(v_arg1)
	ld	de,(v_arg2)
	ld	a,(v_argc)
	cp	3
	jr	c,ztok2		; < 3 arguments
	ld	bc,(v_arg3)
	jr	z,ztok3		; 3 arguments
	ld	a,(v_arg4)	; 4 arguments
	jp	tokenise
ztok2:
	ld	bc,0
ztok3:
	ld	a,0
	jp	tokenise


; @encode_text zscii-text length from coded-text
zencode:
	call	parse_var_get
	ld	hl,(v_arg1)	;Text in
	ld	de,(v_arg3)	;offset
	add	hl,de
	ld	a,(v_arg2)	;Length
	ld	b,a
	call	encode
	ld	a,(encptr)
	ld	hl,(v_arg4)	;Destination
	ld	de,encbuf
	or	a
;zenc1:	or	a
zenc1:
;zi	scf
	ret	z
	ex	de,hl		;Transfer 3 Z-characters, HL now has source
	ld	b,(hl)		;Put 2 bytes (3 Z-characters) into BC
	inc	hl		;Transfer 3 Z-characters
	ld	c,(hl)
	ex	de,hl		;HL now has destination
	call	zxpokw		;write to destination
	inc	hl
	inc	hl
	inc	de
;	pop	af
	sub	3		;3 characters moved
	ret	c		;also zero flag has been updated
	jp	zenc1		; so no need to check again.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Scan table for byte or word pattern
; @scan_table x table len form -> (result)
scantbl:
	call	parse_var_get
	ld	a,(v_argc)
	cp	4
	ld	a,82h
	jr	c,scant1
	ld	a,(v_arg4)
scant1:
;	bit	7,a
;	jr	z,scanbyt
	or	a
	jp	p,scanbyt	;fast check for bit 7. P = reset (i.e. accumulator is positive)
	and	7Fh	;<< v1.10: Only bits 0-7 are entry size >>
;	ld	e,a
;	ld	d,0	;DE = step between table entries
	ld	e,0	;EHL holds address
	ld	d,a	;D has step between table entries.
	ld	bc,(v_arg3)
	ld	a,b
	or	c
	jr	z,tnmatch	;if it's a zero length table, trivial no-match
;	ld	hl,(v_arg2)
	ld	hl,(v_arg1)
;	ld	a,(v_arg1)
	ld	a,l
	ld	(scanw_cp1),a
;	ld	a,(v_arg1+1)
	ld	a,h
	ld	(scanw_cp2),a
	ld	hl,(v_arg2)
scanw1:
;	push	de	; replace with zxpkwd, neeed to save BC as well etc.
	push	bc	; preserve counter- can't use BC' around ZX call.
;	ld	a,e	;4
;	ld	e,0	;7
	call	ZXPKWD
;	ld	e,a	;4
;	ld	de,(v_arg1)
;	call	cpdebc

	ld	a,c	;Tested correctly - DOTR/Z5
	cp	0	;updated on the fly to first byte
scanw_cp1	EQU	$-1
	jr	nz,scanw2
	ld	a,b
	cp	0	;updated on the fly to second
scanw_cp2	EQU	$-1
	jr	z,tmatchpop
scanw2:
;	pop	bc
;	ld	a,d	;4
;	add	a,l	;4
;	ld	l,a	;4
;	jr	nc,scanw3	;12/7
;	inc	h	;4 total = 24/23 ; 6 bytes
;scanw3:
;	add	hl,de	;11 total = 26; 5 bytes
	ld	b,e	;4
	ld	c,d	;4	e is 0, d is step so crossing over can add BC
	add	hl,bc	;11 total = 19, 2 bytes
	pop	bc
	dec	bc
	ld	a,b
	or	c
	jr	nz,scanw1
	jr	tnmatch
;
scanbyt:
	ld	e,a
	ld	d,0		;DE = step between table entries
	dec	de		;clean up auto increment
	ld	bc,(v_arg3)	;Table length
	ld	a,b
	ld	b,c
	ld	c,a		;now b and c are swapped
	or	b
	jr	z,tnmatch	;if it's a zero length table, trivial no-match
;	ld	hl,(v_arg2)
;	ld	a,(v_arg1+1)
	ld	hl,(v_arg1)	;1 byte shorter, 2T faster
	ld	a,h
	and	a
	jr	nz,tnmatch	;must fail if searching for value>=100h
;	ld	a,(v_arg1)
	ld	a,l
	ld	(scanb_cp),a
	ld	hl,(v_arg2)
scanb1:
;	push	de		;POSSIBLE SPEEDUP
	call	ZXPK64i		;peek64		;HL, DE, BC preserved
	cp	0
scanb_cp:	EQU	$-1
;	pop	de
	jr	z,tmatchdec
	add	hl,de
;	dec	bc
;	ld	a,b
;	or	c
;	jr	nz,scanb1
	djnz	scanb1		;this method takes more bytes however we saved
	ld	a,c		;so much by having byte with a compulsory increment
	or	a		;which helps make the payoff worthwhile
	jr	z,tnmatch
	dec	c
	jr	scanb1
tnmatch:
	ld	bc,0
	call	ret_bc
	jp	nbranch

tmatchdec:
	dec	hl
	call	ret_hl
	jp	branch
;
tmatchpop:
	pop	bc	;even out the stack for the word version
tmatch:	call	ret_hl
	jp	branch
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Copy_table
;
; @copy_table first second size
;
; If second is zero, then size bytes of first are zeroed.
;Otherwise first is copied into second, its length in bytes being the absolute
; value of size (i.e., size if size is positive, -size if size is negative).
;The tables are allowed to overlap. If size is positive, the interpreter must
; copy either forwards or backwards so as to avoid corrupting first in the
; copying process. If size is negative, the interpreter must copy forwards
; even if this corrupts first.
;
cpytab:
	call	parse_var_get
	ld	hl,(v_arg2)	;Destination
	ld	bc,(v_arg3)	;Count
	ld	a,b		;<< v1.11
	or	c		;If count=0 (vacuous) do nothing
;zi	scf			;rather than copy 64k bytes.
	ret	z		;>> v1.11

	ld	a,l
	or	h
	jr	z,zerotab
	ld	de,(v_arg1)	;Source
;	ld	bc,(v_arg3)	;Count
;	ld	a,b		;<< v1.11
;	or	c		;If count=0 (vacuous) do nothing
;	scf			;rather than copy 64k bytes.
;	ret	z		;>> v1.11
	bit	7,b
	jr	nz,fwdcpy	;changes sign
;
;If source < dest, use backward copy
;
	call	cphlde
	jr	c,fwdcpylp	;don't need to change sign
bwdcpy:	add	hl,bc
	ex	de,hl
	add	hl,bc
;	ex	de,hl
	ld	a,c	;+3 bytes - swap so we can use djnz on inner loop
	ld	c,b
	ld	b,a
	inc	c	;offset by 1 here to save 1 byte and 8 cycles per outer loop
bcpylp:	dec	de
	dec	hl
;	ex	de,hl
	call	ZXPK64i	;peek64
	dec	HL	;due to auto increment
	ex	de,hl
	call	zxpoke
	ex	de,hl	;for next loop round. This way saves 1 byte and 4 cycles coming in.
	djnz	bcpylp	;+8 bytes
;	ld	a,c
;	or	a
	dec	c
	ret	z
;	dec	c
	jr	bcpylp
;	dec	bc	;-7 bytes
;	ld	a,b
;	or	c
;	jp	nz,bcpylp	;use jp so faster on average (usually NZ)
;zi	scf
;	ret
;
fwdcpy:	call	negbc	;was absbc
fwdcpylp:
	ex	de,hl
	call	ZXPK64i	;Read from source
	ex	de,hl
	call	zxpoke	;Write to dest
	inc	hl
;	inc	de	;from above
	dec	bc
	ld	a,b
	or	c
	jp	nz,fwdcpylp	;use jp faster on average
;zi	scf
	ret
; BC already loaded and confirmed not zero from copy above
zerotab:
	ld	hl,(v_arg1)
;	ld	bc,(v_arg3)	;Count - rewritten to check both above
;	ld	a,b		;<< v1.11
;	or	c		;If count=0 (vacuous) do nothing
;	scf			;rather than copy 64k bytes.
;	ret	z		;>> v1.11
	call	absbc
zerot1:	xor	a
	call	ZXPOKE
	inc	hl
	dec	bc
	ld	a,b
	or	c
	jp	nz,zerot1	;assume likely >1
;zi	scf
	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
prtbl:
	call	parse_var_get
	call	flush_buf	;<< v0.04 >> Ensure the screen is up to date


	ld	a,(v_argc)
	cp	4
	jr	z,prtbl0
	ld	hl,0
	ld	(v_arg4),hl
	cp	3
	jr	nc,prtbl0	;<< v0.04 >> If there is 1 line, initialise
	inc	hl
;	ld	hl,1		;            properly
	ld	(v_arg3),hl	;<< v1.11 >> A=2 - store HL!
prtbl0:	ld	hl,(v_arg3)	;<< v1.04 deal with degenerate case
	ld	a,h		;        when height is 0
	or	l
;zi	scf
	ret	z		;>> v1.04
;
; Get current cursor position
;
	call	ZXGETX
	ld	e,l
	call	ZXGETY
	ld	d,l	;D=Y E=X
	ld	bc,(v_arg2) 	;<< v1.02 rewritten for lines
	ld	hl,(v_arg1)	;        longer than 256 chars
	ld	a,b		;<< v1.04 deal with degenerate case
	or	c		;        when width is 0
;zi	scf
	ret	z		;>> v1.04
	xor	a		;<< v1.11 >> Move this xor a down so that
				;A really is zero on the first line.
prtbl1:
	push	de
	call	doline
	ld	de,(v_arg4)
	add	hl,de
	pop	de
	inc	a
	inc	d	;Next line
	push	hl
	ld	hl,v_arg3
	dec	(hl)
	pop	hl
	jr	nz,prtbl1	;>> v1.02
	jp	flush_buf	;<< v1.11 >>
;zi	scf
;zi	ret
;
doline:
;	push	de	;<< v1.02 rewritten for lines >256 chars - DE saved above.
;	push	bc
	or	a	;<< v0.04  Don't position the cursor if on 1st line
	jr	z,doln1	;>> v0.04
	ld	b,d
	ld	c,e
;	inc	b
;	inc	c	;1-based - already now returned as 1-based, so reverted back to as-is.
	push	hl	;<< v1.11 ZXSCUR is allowed to trash HL!
	call	ZXSCUR
	pop	hl	;>> v1.11
;	pop	bc
;	push	bc
doln0:	ld	bc,(v_arg2)	;line length
doln1:	push	bc
	call	ZXPK64i	;ZSCII character and increment address
	push	hl
	ld	l,a
	ld	h,0
	call	ll_zchr	;
	pop	hl
;	inc	hl	;HL = address of character to read
	pop	bc
	dec	bc
	ld	a,b
	or	c
	jr	nz,doln1

;	pop	bc
;	pop	de	;>> v1.02
	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; << v1.10: New line conversion functions
;
; Convert line from v3 format to v5.
;
line_to_v5:
	ld      hl,(v_arg1)     ;input buffer
        inc     hl              ;Length
        call    ZXPK64rw         ;A = actual length
        or      a
	ret	z		;No input; the 0 becomes a terminating null
        ld      b,a
sreadv: inc     hl
        call    ZXPK64rw          ;Character
        call    valid_char      ;Ensure it's valid
	dec	hl
        call    ZXPOKE		;Write it to previous slot
	inc	hl
        djnz    sreadv
sreadw:
	xor	a
	jp	ZXPOKE		;Terminating 0
;
; Convert line from v5 format to v3. Only used when recovering from a
; timeout.
;
line_from_v5:
	ld	hl,(v_arg1)
	inc	hl
	ld	bc,0		;B = length, C = character being moved up
lv501:	call	ZXPK64rw
	or	a
	jr	z,l5eol
	ld	e,a		;E = character just read
	ld	a,c
	call	ZXPOKE		;Write character from previous slot
	ld	c,e		;C = next character to write back
	inc	hl
	inc	b
	jr	lv501
;
l5eol:	ld	a,c
	call	ZXPOKE		;Write back the last character
	ld	hl,(v_arg1)
	inc	hl
	ld	a,b		;A = count
	jp	ZXPOKE
;

