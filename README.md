![M4ZVM Logo](m4zvm_model_4_logo_I.jpg)

New Release 18. Published 09-feb-2025 - check Changelog. <br>
New Release 17. Published 07-jan-2024 - check Changelog.  <br>

Also available: [M1ZVM](https://gitlab.com/sijnstra1/m1zvm/) for Model I [M3ZVM](https://gitlab.com/sijnstra1/m3zvm/) for Model III

Pre-built bootable disk images along with donations/thank yous can be made through my page on itch.io [M4ZVM](https://sijnstra.itch.io/m4zvm)

For a CP/M version with the same core, use [Vezza](https://gitlab.com/sijnstra1/vezza/)

The Vezza core has also been adapted to run as an all-in-one embedded application, i.e. with the game file included in a single blob where mass storage is not available. Embedded versions for 48k/128k tape Spectrum & TEC-1G are here - [e-VEZZA](https://gitlab.com/sijnstra1/e-vezza)

There is also an advanced version available on itch.io for the Agon Light & Console 8 family of computers. [Vezza-Agon](https://sijnstra.itch.io/vezza-agon)

# Table of Contents

[[_TOC_]]

# Technical summary
~~~
M4ZVM is a TRS-80 Model 4 (128K recommended) Infocom/Inform/Z-machine game
    virtual machine. It's a port and enhancement of ZXZVM for the Spectrum
    and Amstrad https://gitlab.com/garrylancaster/zxzvm
Model 1 - M1ZVM - and Model 3 - M3ZVM - versions also availale see links above
It can run v1-v8* inform format interactive fiction game files including
    Infocom and post-Infocom era story game files.
    *see features and limitations below
    Interactive Fiction games are available at numerous locations including:
    https://www.ifarchive.org/ just look for files with extension such as 
    .z1, .z2, .z3, .z4, .z5, .z6, .z7, .z8
The binaries in the zout directory here are built to run on TRSDOS 6.2 or LS-DOS 6.3.x
**NOTE for REAL HARDWARE**
         While this software is intended to and does run on real hardware,
it is possible that some newer post-Infocom games, using a real floppy disk drive
where the game does not load entirely in to memory, are written in a way that causes
the drive to seek quite a lot. If you are concerned, try it on an emulator first or
use your judgement.
~~~
# Support and Suggestions
~~~
Written by Shawn Sijnstra (c) 2021-2 under GPLv2
If you would like to donate or provide a thank you, you can do so through the
download link at https://sijnstra.itch.io/m4zvm
Feedback, comments and bug reports welcome - email me <firstname>@<surname>.com
About me: home page of my various projects and other work http://sijnstra.com
~~~
# Inspiration and the Games
~~~
It has been a very long time since I was first annoyed that a text adventure of all things
would no longer be developed and run on my favourite computing platform, the System-80
(TRS-80 model 1 clone). Why, Infocom, did you abandon me? Commercial reality I expect....
I had learned about the existance of ZXZVM and since the late 90s, and had plotted
to port it but never really got to it as I couldn't see the pieces come together.
In 2020 I stumbled upon Office Goose, a small and fun adventure that reminded me again of
how annoyed I was at the lack of later Infocom games missing from my formitive computing
expierience.

Then it all came together... I had acquired the model 4p already (thanks Dad!), acquired
a number of hardware upgrades, pulled together a really nice development environment,
had some time on my hands due to the lockdown and cracked on with it.
4 weeks of programming and testing, and here we are!

If you're unfamiliar with interactive fiction, there's some good collections of
introductory material at http://brasslantern.org/beginners/ and 
https://www.ifwiki.org/index.php/Starters

If you're wondering where you can find the game data files, there's many places online
inluding the Obsessively Complete Infocom Catalog https://eblong.com/infocom/ which
will get you a good start. Game manuals are available elsewhere including abandonware
sites. I also recommend diving into the z-code part of the Interative Fiction archive
and start with the .z2/.z3/.z4/.z5 games (see below for more guidance)
https://www.ifarchive.org/indexes/if-archive/games/zcode/
~~~
# Features
~~~
Supports the core features needed including
* z1, z2, z3, z4, z5, z6, z7 and z8 games can execute
* Supports full 80x24 display (no trimmed last column unlike others)
* Named game save and load
* Timed input (to support game such as BorderZone as well as some non-adventure games)
* Transcript (printing)
* Reverse text highlighting, including when selected using colour changing codes
* Accented chatacters
* Fast execution of games (from Release 10, z3 games now execute faster than the original)
* z3 status line supports am/pm time including Cut Throats easter egg
* Supports as much memory as your computer/drivers support (128k machine minimum recommend)
  - e.g. An upgraded model 4 with 384K will load up to 320k games entirely into RAM
* Separate version/memory map for 64K and 128k+ versions
* 128K version: Supports any valid Z-code 'Dynmem' dynamic memory size (i.e. up to 64k)
* 128K version: 21KB Least-recently-used disk cache to minimise disk I/O
* 64K version supports up to 22K DynMem (won't load Trinity or Beyond Zork for example)
* 4KB z-machine stack size (over 100 call levels deep)
* Uses SVCs for high memory and IO, meaning you can
  - Take advantage of any high memory such as XLR8er or Hypermem with the drivers present
  - Redirect *DO and *PR to change input and transcript output
* Split window including full support for Seastalker
* arrows recongnised by game (some prompts now need shift-clear or shift-left to delete
  text such as Bureaucracy - this happens for character based input fields)
* F1,F2,F3, shift F1, shift F2, shift F3 map to F1-F6 for Beyond Zork shortcuts
  (type DEFINE within the game to set the shortcuts)
* Shift-Enter maps to ^ to support Bureaucracy
* Ctrl-A, Ctrl-F, Ctrl-G, Ctrl-H to edit via cursor left, cursor right, delete, backspace
  shift-left and shift-right also move the cursor left and right during line input
* Built-in z-code debug feature (see debug source file - can enable at any time by setting
  the "trace" bit which is bit 7 in the "running" flag)
* command-line switch to optionally enable the infamous "Tandy bit". This will tweak the
  messages and the copyright block of some z3 games.
For release by release update technical details, check out the Changelog.
~~~
# Limitations and known bugs
~~~
* Large and complex .z8 & .z5 games can execute slowly, especially if they use the inform7
  compiler or use the interpreter in a way that exceeds the compute power of 8 bits.
  The easiest rough guide is that post-infocom games are much more likely to run well if
  compiled using Inform6, especially the PunyInform library. Inform7 games are likely to
  be too complex.
* WARNING and Special note for *Inform 7* compiled games: The Inform 7 library is
  designed to use the stack a lot more than either Inform 6 or any of the genuine Infocom
  games. This means that even if an Inform 7 compiled game loads, and you are patient enough
  to wait for your next turn, as the game develops it may reach a point where the game runs
  out of stack and you can no longer progress that story. This would be disappointing to
  say the least.
* z6 support is very minimal and largely proof of concept only. The main games which are
  playable are Arthur and Shogun, noting the graphics are not supported. Unreleased games
  such as the German version of Zork I and Restaurant at the End of the Universe also work.
  Zork 0 is now somewhat playable. Journey is not.
  IMPORTANT NOTE: No graphics support also means that where the games have a graphic puzzle,
  that the puzzle will not be visibile. This includes one in Shogun, and multiple in Zork 0.
* Other highlighting techniques not supported (italic, bold)
* Only one font (font 2 and font 3 not supported)
* Accented characters supported within hardware limitations, a default M4 and a default M4p
  mapping is included. Remaps characters outside of the font to make them displayable.
* Sound, graphics not supported (although tempted to look at HR graphics support for splash)
* No support for Undo (note that M4ZVM correctly reports this)
* No Colour or greyscale other than mapping to plain or reverse text where possible
* Transcript is to printer only, assumes same width as screen, and doesn't convert accented characters
* Upper window does not auto-expand, however, it does allow the text to be displayed regardless
* Testing has largely been by me on a TRS-80 4p with XLR8r, and on the GP2000 emulator
  so more testing and bug reports welcome!
* in Release 6+ of the interpreter, it uses a mix of @BANK SVC and direct memory switching
  which works on 128k machines as well as with the XLR8er as well as George Phillips'
  update to the HyperBnk driver for HyperMem. The Release 5 branch is kept in case this
  multi-mode version doesn't work.
  Let me know if you are impacted and I will look at backporting other improvements.
~~~
# Screenshots
~~~
Curses (z5 game) running on trs80gp:
~~~
![Curses screenshot](m4zvm_curses.z5_trs80-0.gif "Curses (z5 game) running on trs80gp")
~~~
Bureaucracy (z4 game) running on trs80gp:
~~~
![Bureaucracy screenshot](Bureaucracy_Capture.PNG "Bureaucracy (z4 game) running on trs80gp")
~~~
Office Goose (z8 game) on actual model 4p:
~~~
<img src="Model4p_Goose_DSGF8886_crsm.jpg" alt="Office Goose screenshot" width=500>

# Classic Infocom Compatibility guide
~~~
Beyond the rich variety of working post-Infocom games referred to above such as
 PunyInform games, this list gives a guide on which classic Infocom games can
 now be played on which TRS-80 model. Anything listed from z4 onwards is what
 is totall new to the TRS-80 as a result of this software.

		 Model I/III  Model 4 (64k)  Model 4 (128k)
z1 release:
original zork 1		y	    y		y
z3 (standard) series:
all standard series     y(sonar*)   y		y
z4 (plus) series:
bureaucracy		-(screen*)  y           y
Trinity			-           -		y
Nord and Bert		y(narrow)   y		y
AMFV			-	    -		y
z5 series:
Sherlock		y	    y		y
Beyond Zork		-(screen*)  y           y
Border Zone		y	    y		y
ZTUU			y	    y		y
invisiclues		y(cosmetic) y		y
z6 series (graphics):
Arthur			*	    *		*
Shogun			*	    *		*
Journey			-	    -		- (requires graphics)
Zork 0			-	    -		- (requires graphics)
Unreleased z6 series (text only): 
Restaurant		y	    y		y
German Zork 1		y*	    y		y

NOTES:
z3:
Note that the standard series including Zork 1/2/3, Wishbringer, Planetfall, Deadline,
 Cutthroats etc. were all able to run on the infocom created interpreter
z3 standard standard games work well, however, the extra screen height on the model 4
 allows for the sonascope to be displayed as intended in a split window. Model I/III
  do not have enough rows on the screen.
z4:
Bureacracy screen* - the screen height (not enough rows) is the main challenge which
 stops the opening sequence from working.
Nord and Bert (narrow) - it's recommended to have a modified narrow screen version of
 this game on Model 1/III so that the list of directions isn't truncated.
 For narrow screen editions, use the releases under
 https://github.com/ChristopherDrum/status-line-classics
Trinity and AMFV both use a memory footprint that requires a 128k model 4.
z5:
Beyond Zork as released requires the screen height of the model 4. Invisiclues games
 work on all models, noting that on the model I/III some pages the list of clue topics
  might be too long for the screen.
z6:
Arthur & Shogun - Arthur works in full although with no graphics, and Shogun works
 until a graphics puzzle (around half way)
The rest of the games listed work as expected, noting that the Model 1 does no support
 the extra accented characters that the Model III/4 support.
~~~
# Timeline
~~~
Some of you might be wondering how long did this project take? Here's a rough guide:
From actual coding to initial release: Around 4 weeks (over 6 months now in tuning)
From recent inspirations to having a working build environment (virtual) and a
  live fully upgraded TRS-80: Around 2 years
From first seeing the ZXZVM code: Maybe 20 years?
From first being annoyed that I couldn't run Bureaucracy on a TRS-80: Maybe 30 years?
~~~
# Credits and thanks
~~~
John Elliot who created and shared the ZXZVM code https://www.seasip.info/ZX/zxzvmport.html
And Garry Lancaster who made it even more portable https://gitlab.com/garrylancaster/zxzvm
George Phillips et al who created zmac http://48k.ca/zmac.html 
  and the all purpose TRS-80 emulator trs80gp http://48k.ca/trs80gp.html
  as well as providing support on hyperbnk and the detecting Model 4/4p tip
Fredrik Ramsberg for sharing his knowledge and experience from his own Ozmoo interpreter
  build experience https://github.com/johanberntsson/ozmoo
  as well as convincing me to work hard on improving the performance of the interpreter.
Ian Mavrick, Arno Puder, Jean-François DEL NERO for their hardware contrubtions, making
  these old machines easier to use in a modern world - between the 4Cellerator, TRS-IO
  and HxC2001 I have a faster CPU and more memory, access to transfer files over wifi,
  and a floppy drive emulator to give me reliability.
A shout out to people who have provided me feedback to keep making this better - especially
  Jonathan Hoof who has provided an amazing amount of feedback and testing, helping to
  make this as fast and reliable as it can be!

The Retrocomputing community for making documentation available including
  BDOS codes http://members.iinet.net.au/~daveb/cpm/bdos.html
  Amstrad and TRS-80 programming manuals on archive.org
  Tim Mann and Roy Soltoff for making the LS-DOS documentation and software available
  Ira Goldklang and so many others for hosting and curating TRS-80 information
SYDTRUG and its members over the years for getting me into the TRS-80 in the first place
  and supporting my curiosity. It's also the place where I first got the inspiration to
  want to do this so many years ago.
And of course Infocom and really so many others who made this all possible....
~~~
